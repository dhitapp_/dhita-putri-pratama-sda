import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SDA18192T {
    BufferedReader reader;
    int jumlah, maks, events,after, element;
    String[] two_inst, data;
    Marble[] marbles;
    MarbleGame game;
    public SDA18192T() throws IOException{
        /*
        Menerima input
         */
        reader = new BufferedReader(new InputStreamReader(System.in));
        two_inst = reader.readLine().split(" ");
        maks = Integer.parseInt(two_inst[1]);
        data = reader.readLine().split(" ");

        //Create Doubly Circular Linked List
        marbles = new Marble[data.length];
        marbles[data.length-1] = new Marble(Integer.parseInt(data[data.length-1]), null, null);
        for (int i = data.length-2; i >= 0; i-- ) {
            marbles[i] = new Marble(Integer.parseInt(data[i]), marbles[i+1], null);
        } marbles[data.length-1].next = marbles[0];
        marbles[0].prev = marbles[data.length-1];
        for (int i = 1; i < data.length; i++ ) {
            marbles[i].prev = marbles[i-1];
        }

        //Create the Game
        game = new MarbleGame(marbles[0]);
        game.checkAndDelete();
        jumlah = game.countElements();
        if (jumlah < 4) {
            System.out.println("MENANG");
            return;
        }

        // Receive events

        events = Integer.parseInt(reader.readLine());
        for (int i = 0; i < events; i++) {
            two_inst = reader.readLine().split(" ");
            after = Integer.parseInt(two_inst[0]);
            element = Integer.parseInt(two_inst[1]);
            game.insertMarble(after, element);
            game.checkAndDelete();
            jumlah = game.countElements();
            if (jumlah < 4) {
                System.out.println("MENANG");
                return;
            } else if (jumlah > maks) {
                System.out.println("KALAH");
                return;
            }
        }  System.out.println(jumlah);
    }

    public static void main(String[] args) throws IOException{
        new SDA18192T();
    }
}

class Marble {

    Marble next, prev;
    int content;


    public Marble(int cont, Marble ref, Marble bef) {
        content = cont;
        next = ref;
        prev = bef;
    }
}

class MarbleGame {
    Marble head, start, end;
    int count;

    public MarbleGame(Marble head) {
        this.head = head;
    }
    /*
    cek apakah marbel tersebut memiliki elemen sama di marbel setelah dan sebelumnya.
    Jika yang sama ada lebih dari 3, buang dari linked list
     */
    public void checkAndDelete() {
        Marble current = head;
        while (current.next != head) {
            int result = checkNextMarble(current) + checkPrevMarble(current);
            if (result >= 3) {
                if (current == head) {
                    head = end;
                    current = head.prev;
                }
                start.next = end;
                end.prev = start;
            }
            current = current.next;
        }
    }

        public int countElements() {
        int counter = 0;
        Marble current = head;
        while (current.next != head) {
            counter++;
            current = current.next;
        } if (current.next == head) {
            counter++;
        } return counter;

    }
    public void printElements() {
        Marble current = head;
        while (current.next != head) {
            System.out.print(current.content + " ");
            current = current.next;
        } System.out.println(current.content);
    }

    /*
    Memeriksa berapa banyak elemen yang sama setelah elemen tersebut
     */
    public int checkNextMarble(Marble current) {
        int jml = 1;
        Marble temp = current;
        end = current.next;
        while (current.next != head) {
            if (temp.content == current.next.content) {
                jml++;
                current = current.next;
                end = current.next;
            } else return jml;
        } if (temp.content == current.next.content) {
            jml++;
            end = current.next;
        } return jml;
    }
    /*
    Memeriksa berapa banyak elemen yang sama sebelum elemen tersebut
     */
    public int checkPrevMarble(Marble current) {
        int jml = 0;
        Marble temp = current;
        start = current.prev;
        while (current.prev != head) {
            if (temp.content == current.prev.content) {
                jml++;
                current = current.prev;
                start = current.prev;
            } else return jml;
        } if (temp.content == current.prev.content) {
            jml++;
            start = current.prev;
        } return jml;
    }

    /*
    Masukkan marbel pada posisi yang diinginkan
     */
    public void insertMarble(int loc, int element) {
        int counter = 0;
        Marble current = head;
        if (loc == 0){
            Marble rec = new Marble(element, head, head.prev);
            rec.prev.next = rec;
            rec.next.prev = rec;
            head = rec;
        }
        else if (loc > 0) {
            while (current.next != head) {
                counter++;
                if (counter == loc) {
                    Marble recent = new Marble(element, current.next, current);
                    current.next.prev = recent;
                    current.next = recent;
                    break;
                } current = current.next;
            } if (current.next == head) {
                Marble recent = new Marble(element, current.next, current);
                current.next = recent;
                head.prev = recent;
            }
        }
    }

}
